-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2018 at 03:39 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newhorizon`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_attendance` (IN `client_id` INT, IN `schedule_id` INT, IN `date` DATETIME)  NO SQL
INSERT INTO `attendance`(`client_id`, `schedule_id`, `date`) VALUES (client_id,schedule_id,date)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_new_client` (IN `pass` VARCHAR(400), IN `type` VARCHAR(400))  NO SQL
INSERT INTO `client`(`client_id`, `client_password`, `type`) SELECT Max(person.id), pass, type FROM person$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_new_course` (IN `name` VARCHAR(400), IN `code` VARCHAR(400), IN `category` VARCHAR(400), IN `prerequisites` TEXT, IN `description` TEXT, IN `outline` TEXT, IN `include` VARCHAR(400), IN `hours` INT, IN `vendor` VARCHAR(255), IN `level` VARCHAR(255), IN `cost` INT, IN `discount` DECIMAL)  NO SQL
INSERT INTO course (`course_name`, `course_code`, `course_category`, `course_prerequisites`, `course_description`, `course_outline`, `course_include`, `course_hours`, `course_vendor`, `course_level`, `course_cost`, `course_discount`) VALUES (name, code, category, prerequisites, description, outline, include, hours, vendor, level, cost, discount)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_new_instructor` (IN `cv` VARCHAR(400))  NO SQL
INSERT INTO `instructor`(`instructor_id`, `instructor_cv`) SELECT Max(person.id), cv FROM person$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_new_person` (IN `name` VARCHAR(400), IN `phone` VARCHAR(400), IN `email` VARCHAR(400))  NO SQL
INSERT INTO `person`(`name`, `phone`, `email`) 
VALUES (name, phone, email)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_new_sales` ()  INSERT INTO `sales`(`sales_id`) SELECT Max(person.id) FROM person$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_new_schedule` (IN `course_id` INT, IN `branch` VARCHAR(255), IN `lab` INT, IN `start_date` DATETIME, IN `end_date` DATETIME, IN `time` TIME)  NO SQL
INSERT INTO `schedule`(`course_id`, `branch_name`, `lab_num`, `start_date`, `end_date`, `start_time`) VALUES (course_id, branch, lab, start_date, end_date, time)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_registeration` (IN `client_id` INT, IN `schedule_id` INT, IN `course_id` INT)  NO SQL
INSERT INTO `registeration`(`client_id`, `course_id`, `schedule_id`) VALUES (client_id, course_id, schedule_id)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_course_by_ID` (IN `ID` INT)  NO SQL
DELETE FROM course
WHERE course.course_id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_person_by_phone` (IN `phone` VARCHAR(400))  NO SQL
DELETE FROM person
WHERE person.phone = phone$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_registeration` (IN `client_id` INT, IN `schedule_id` INT, IN `course_id` INT)  NO SQL
DELETE FROM registeration
WHERE registeration.course_id = course_id & registeration.schedule_id = schedule_id & registeration.client_id = client_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_schedule` (IN `ID` INT)  NO SQL
DELETE FROM schedule 
WHERE schedule.id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_categories` ()  NO SQL
SELECT DISTINCT course.course_category FROM course
WHERE course.course_category IS NOT NULL$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_clients` ()  NO SQL
SELECT client.client_id, person.name, person.phone, person.email, client.client_password, client.type
FROM client, person
WHERE person.id = client.client_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_courses` ()  NO SQL
SELECT *
FROM course$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_instructors` ()  NO SQL
SELECT instructor.instructor_id, person.name, person.phone, person.email, instructor.instructor_cv
FROM instructor, person
WHERE person.id = instructor.instructor_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_sales` ()  NO SQL
SELECT sales.sales_id, person.name, person.phone, person.email
FROM sales, person
WHERE person.id = sales.sales_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_vendors` ()  NO SQL
SELECT DISTINCT course.course_vendor FROM course
WHERE course.course_vendor IS NOT NULL$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_available_courses` ()  NO SQL
SELECT course.course_id, course.course_name, course.course_code, course.course_category, course.course_prerequisites, course.course_description, course.course_outline, course.course_include, course.course_hours, course.course_vendor, course.course_level, course.course_cost, course.course_discount
FROM course
INNER JOIN schedule on course.course_id = schedule.course_id
WHERE CURRENT_TIMESTAMP() BETWEEN schedule.start_date AND schedule.end_date$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_client_by_email_password` (IN `email` VARCHAR(400), IN `password` VARCHAR(400))  NO SQL
SELECT person.id, person.name, person.phone , person.email, client.client_password, client.type 
FROM client, person

WHERE person.email = email AND client.client_password = password AND person.id = client.client_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_client_by_ID` (IN `ID` INT)  NO SQL
SELECT person.id, person.name, person.phone, person.email, client.client_password, client.type
FROM person
INNER JOIN client on person.id = client.client_id
WHERE person.id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_client_by_phone` (IN `phone` VARCHAR(400))  NO SQL
SELECT person.id, person.name, person.phone, person.email, client.client_password, client.type
FROM person
INNER JOIN client on person.id = client.client_id
WHERE person.phone = phone$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_courses_by_branch` (IN `branch` VARCHAR(255))  NO SQL
SELECT course.course_id, course.course_name, course.course_code, course.course_category, course.course_prerequisites, course.course_description, course.course_outline, course.course_include, course.course_hours,course.course_vendor, course.course_level, course.course_cost, course.course_discount, schedule.lab_num, schedule.start_date, schedule.end_date, schedule.start_time
FROM course
INNER JOIN schedule on course.course_id = schedule.course_id 
WHERE branch_name = branch$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_courses_by_category` (IN `category` VARCHAR(400))  NO SQL
SELECT * FROM course WHERE course_category = category$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_courses_by_client` (IN `ID` INT)  NO SQL
SELECT course.course_id, course.course_name, course.course_code, course.course_category, course.course_prerequisites, course.course_description, course.course_description, course.course_outline, course.course_include, course.course_hours,course.course_vendor, course.course_level, course.course_cost, course.course_discount
FROM course
INNER JOIN registeration on course.course_id = registeration.course_id
WHERE registeration.client_id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_courses_by_code` (IN `code` VARCHAR(400))  NO SQL
SELECT * FROM course WHERE course_code = code$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_courses_by_name` (IN `name` VARCHAR(400))  NO SQL
SELECT * FROM course WHERE course.course_name = name$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_courses_by_vendor` (IN `vendor` VARCHAR(255))  NO SQL
SELECT * FROM course WHERE course_vendor = vendor$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_course_by_ID` (IN `ID` INT)  NO SQL
SELECT * FROM course
WHERE course.course_id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_enrolled` (IN `ID` INT)  NO SQL
SELECT person.id, person.name, person.phone, person.email, client.client_password, client.type
FROM person, client
INNER JOIN registeration ON client.client_id = registeration.client_id
WHERE client.client_id = person.id AND registeration.course_id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_enrolled_courses` (IN `client_ID` INT)  NO SQL
SELECT C.course_id, C.course_name, C.course_code, C.course_category, C.course_prerequisites, C.course_description, C.course_outline, C.course_include, C.course_hours, C.course_vendor, C.course_level, C.course_cost, C.course_discount 

FROM course AS C

INNER JOIN registeration on C.course_id = registeration.course_id
INNER JOIN client ON client.client_id = registeration.client_id
INNER JOIN schedule ON schedule.id = registeration.schedule_id

WHERE schedule.end_date > CURRENT_DATE
AND
client.client_id = client_ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_finished_courses` (IN `client_ID` INT)  NO SQL
SELECT C.course_id, C.course_name, C.course_code, C.course_category, C.course_prerequisites, C.course_description, C.course_outline, C.course_include, C.course_hours, C.course_vendor, C.course_level, C.course_cost, C.course_discount 

FROM course AS C

INNER JOIN registeration on C.course_id = registeration.course_id
INNER JOIN client ON client.client_id = registeration.client_id
INNER JOIN schedule ON schedule.id = registeration.schedule_id

WHERE schedule.end_date < CURRENT_DATE
AND
client.client_id = client_ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_instructor_by_ID` (IN `ID` INT)  NO SQL
SELECT person.id, person.name, person.phone, person.email, instructor.instructor_cv
FROM person
INNER JOIN instructor on person.id = instructor.instructor_id
WHERE person.id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_instructor_by_phone` (IN `phone` VARCHAR(400))  NO SQL
SELECT person.id, person.name, person.phone, person.email, instructor.instructor_cv
FROM person
INNER JOIN instructor on person.id = instructor.instructor_id
WHERE person.phone = phone$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_sales_by_ID` (IN `ID` INT)  NO SQL
SELECT person.id, person.name, person.phone, person.email
FROM person
INNER JOIN sales on person.id = sales.sales_id
WHERE person.id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_sales_by_phone` (IN `phone` VARCHAR(400))  NO SQL
SELECT person.id, person.name, person.phone, person.email
FROM person
INNER JOIN sales on person.id = sales.sales_id
WHERE person.phone = phone$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_schedule_by_ID` (IN `ID` INT)  NO SQL
SELECT * FROM schedule WHERE schedule.id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_totalcost_of_category` (IN `category` VARCHAR(400))  NO SQL
SELECT SUM(course_cost) - (0.15 * SUM(course_cost)) AS "Total Cost"
FROM course
WHERE course_category = category$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `search_courses` (IN `value` VARCHAR(400))  NO SQL
SELECT * FROM course 
WHERE course.course_name = value 
OR course.course_category = value
OR course.course_vendor = value$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_client` (IN `password` VARCHAR(400), IN `type` VARCHAR(400), IN `ID` INT)  NO SQL
UPDATE client
SET client.client_password = password, client.type = type
WHERE client.client_id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_course` (IN `ID` INT, IN `name` VARCHAR(400), IN `code` VARCHAR(400), IN `category` VARCHAR(400), IN `prerequ` TEXT, IN `descr` TEXT, IN `outline` TEXT, IN `include` VARCHAR(400), IN `hours` INT, IN `vendor` VARCHAR(255), IN `level` VARCHAR(255), IN `cost` INT, IN `discount` DECIMAL)  NO SQL
UPDATE course
SET course.course_name = name, course.course_code = code, course.course_category = category, course.course_prerequisites = prerequ, course.course_description = descr, course.course_outline = outline, course.course_include = include, course.course_hours = hours, course.course_vendor = vendor, course.course_level = level, course.course_cost = cost, course.course_discount = discount
WHERE course.course_id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_instructor` (IN `ID` INT, IN `cv` VARCHAR(400))  NO SQL
UPDATE instructor
SET instructor.instructor_cv = cv
WHERE instructor.instructor_id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_person` (IN `ID` INT, IN `name` VARCHAR(400), IN `phone` VARCHAR(400), IN `email` VARCHAR(400))  NO SQL
UPDATE person 
SET person.name = name, person.phone = phone, person.email = email
WHERE person.id = ID$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_schedule` (IN `ID` INT, IN `course_id` INT, IN `branch` VARCHAR(400), IN `lab` INT, IN `start_date` DATETIME, IN `end_date` DATETIME, IN `time` TIME)  NO SQL
UPDATE schedule
SET schedule.course_id = course_ID,
schedule.branch_name = branch,
schedule.lab_num = lab,
schedule.start_date = start_date,
schedule.end_date = end_date,
schedule.start_time = time
WHERE schedule.id = ID$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `client_id` int(255) NOT NULL,
  `schedule_id` int(255) NOT NULL,
  `date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `client_id` int(255) NOT NULL,
  `client_password` varchar(400) NOT NULL,
  `type` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`client_id`, `client_password`, `type`) VALUES
(1, 'fdfdf', 'individual'),
(4, 'kkkk', 'Individual'),
(6, 'lmkl755', 'Individual'),
(8, 'hvjjhvhvhjvhjvhj', 'Individual'),
(18, 'uuu', 'individual'),
(19, '64556dsd', 'individual');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `course_id` int(255) NOT NULL,
  `course_name` varchar(400) NOT NULL,
  `course_code` varchar(400) DEFAULT NULL,
  `course_category` varchar(400) NOT NULL,
  `course_prerequisites` text,
  `course_description` text NOT NULL,
  `course_outline` text NOT NULL,
  `course_include` varchar(400) DEFAULT NULL,
  `course_hours` int(255) NOT NULL,
  `course_vendor` varchar(255) DEFAULT NULL,
  `course_level` varchar(255) NOT NULL,
  `course_cost` int(255) NOT NULL,
  `course_discount` decimal(65,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `course_name`, `course_code`, `course_category`, `course_prerequisites`, `course_description`, `course_outline`, `course_include`, `course_hours`, `course_vendor`, `course_level`, `course_cost`, `course_discount`) VALUES
(1, 'C++', '', 'Programming', 'NONE', 'OOP', 'Chap1, 2, 3, 4', '', 50000000, '', '', 600000000, '0'),
(2, 'dcd', 'C23', 'Programming', NULL, 'fvd', 'dd', ';m', 20, 'Microsoft', 'Advanced', 5000, '0'),
(3, 'C#', 'C101', 'programming', '', 'jkbjkbjk', '.mmm,', '', 20, '', 'Beginner', 3500, '0'),
(5, 'Access', 'AcMicrosoft', 'sks', 'ff', 'ff', 'fff', 'fff', 6, 'h', 'dd', 6, '3'),
(10, 'Excel', '', 'Office', '', 'Excel Basics', 'Functions', '', 10, 'Microsoft', 'Beginner', 1500, '0'),
(11, 'Microsoft C#', '505C', 'Training', NULL, 'aaa', 'rr', NULL, 80, 'Microsoft', 'Beginner', 4000, '0'),
(12, 'C#', '505C', 'Microsoft', NULL, 'oo', 'mmm', NULL, 50, 'hjhvjh', 'Beginner', 4000, '0');

-- --------------------------------------------------------

--
-- Table structure for table `instructor`
--

CREATE TABLE `instructor` (
  `instructor_id` int(255) NOT NULL,
  `instructor_cv` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instructor`
--

INSERT INTO `instructor` (`instructor_id`, `instructor_cv`) VALUES
(13, 'NONE'),
(16, '');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` int(255) NOT NULL,
  `name` varchar(400) NOT NULL,
  `phone` varchar(400) NOT NULL,
  `email` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `name`, `phone`, `email`) VALUES
(1, 'Islam', '010000000', 'Islam@gmail.com'),
(4, 'Ahmed', '10500699', 'ahmed@gmail.com'),
(5, 'Ibrahim Ali', '010546465456', 'ibrahimali@gmail.com'),
(6, 'Ibrahim Ali', '010546465456', 'ibrahimali@gmail.com'),
(8, 'Eman ', '011l5648488', 'emanAali@hotmail.com'),
(11, 'Sayed', '011l5648488', 'sayed@gmail.com'),
(13, 'Gamal Abdallah', '464684564', 'gamal@gmail.com'),
(15, 'Hoda', '5646546', 'hoda@hotmail.com'),
(16, 'Amira', '4546546', 'amira@gmail.com'),
(17, 'Amira', '5', ''),
(18, 'Manar', '015556166', 'manar@gmail.com'),
(19, 'Somaya', '01259595', 'somaya@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `registeration`
--

CREATE TABLE `registeration` (
  `client_id` int(255) NOT NULL,
  `course_id` int(255) NOT NULL,
  `schedule_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registeration`
--

INSERT INTO `registeration` (`client_id`, `course_id`, `schedule_id`) VALUES
(1, 1, 2),
(4, 1, 2),
(4, 2, 3),
(6, 1, 2),
(8, 1, 2),
(8, 1, 13),
(8, 2, 14);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `sales_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`sales_id`) VALUES
(11),
(15);

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `id` int(255) NOT NULL,
  `course_id` int(255) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `lab_num` int(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `start_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `course_id`, `branch_name`, `lab_num`, `start_date`, `end_date`, `start_time`) VALUES
(2, 1, 'Abbas Elakkad', 7, '2018-06-02 00:00:00', '2018-12-12 00:00:00', '09:00:00'),
(3, 2, 'Makram', 4, '2017-02-01 00:00:00', '2018-01-01 00:00:00', '03:30:00'),
(13, 1, 'America', 7, '2016-08-01 00:00:00', '2017-07-01 00:00:00', '08:00:00'),
(14, 2, 'ay7aga', 5, '2018-06-01 00:00:00', '2018-10-03 00:00:00', '01:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`client_id`,`schedule_id`),
  ADD KEY `course_id` (`schedule_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`instructor_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`) USING BTREE;

--
-- Indexes for table `registeration`
--
ALTER TABLE `registeration`
  ADD PRIMARY KEY (`client_id`,`course_id`,`schedule_id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `schedule_id` (`schedule_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`sales_id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`course_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `course_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `instructor`
--
ALTER TABLE `instructor`
  MODIFY `instructor_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `attendance_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `attendance_ibfk_2` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `instructor`
--
ALTER TABLE `instructor`
  ADD CONSTRAINT `instructor_ibfk_1` FOREIGN KEY (`instructor_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `registeration`
--
ALTER TABLE `registeration`
  ADD CONSTRAINT `registeration_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `registeration_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `registeration_ibfk_3` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`id`);

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`sales_id`) REFERENCES `person` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
