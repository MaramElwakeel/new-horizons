<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/rtl.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/Style.css">
    
  </head>
  <body>
      <script src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.min.js"></script>
      <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
      <nav class="navbar navbar-expand-lg  navbar-light ">

  <a href="/home"><img src="${pageContext.request.contextPath}/resources/images\small.PNG" ></a>
  
  
</nav>
<div class="main">
    
    
    <div class="container">
<center>
<div class="middle">
      <div id="login">
<br>
<br>
    <a href="/sales/all"><button type="button" class="btn btn-light btn-lg "><b>&nbsp&nbsp &nbsp Sales &nbsp &nbsp&nbsp</b></button></a>


        <div class="clearfix"></div>

      </div> <!-- end login -->
      <div class="logo" >
<a href="/instructors/all"><button type="button" class="btn btn-light btn-lg "><b>Instructors</b></button></a>
          <div class="clearfix"></div>
      </div>
      
      </div>
</center>
    </div>

</div>
  </body>
</html>