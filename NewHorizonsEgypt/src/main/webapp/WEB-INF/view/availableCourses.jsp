<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Main</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">

<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/Style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/rtl.css">

</head>
<body>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">

	<nav class="navbar navbar-expand-lg  navbar-light ">

		<a href="/home"><img
			src="${pageContext.request.contextPath}/resources/images\small.PNG"></a>

		<form class="form-inline foat-right" id="search">
			<input class="form-control " type="search" placeholder="Search"
				aria-label="Search">
			<button class="btn btn-outline-light " type="submit">Search</button>
		</form>

	</nav>
	<div class="table">
		<table class=" table-striped table-bordered">
			<thead class="thead">
				<!-- construct an "add" link with course id -->
				<c:url var="addLink" value="/courses/addCourse">
					<c:param name="courseId" value="${tempCourse.id}" />
				</c:url>
				<tr>
					<th scope="col">ID</th>
					<th scope="col"><b>Name</b></th>
					<th scope="col"><b>Code</b></th>
					<th scope="col"><b>Category</b></th>
					<th scope="col"><b>Prerequisties</b></th>
					<th scope="col"><b>Description</b></th>
					<th scope="col"><b>Outlines</b></th>
					<th scope="col"><b>Course Include</b></th>
					<th scope="col"><b>Hours</b></th>
					<th scope="col"><b>Vendor</b></th>
					<th scope="col"><b>Level</b></th>
					<th scope="col"><b>Cost</b></th>
					<th scope="col"><b>Discount</b></th>
					<th scope="col"><b>Schedule</b></th>
					<th scope="col"><b>Enrolled</b></th>
					<th scope="col"><a href="${addLink}"><img
							src="${pageContext.request.contextPath}/resources/images\add.PNG"></a></th>
				</tr>
			</thead>
			<tbody>

				<!-- loop over and print our courses -->
				<c:forEach var="tempCourse" items="${Courses}">

					<!-- construct a "schedules" link with course id -->
					<c:url var="schedulesLink" value="/courses/schedules/all">
						<c:param name="courseId" value="${tempCourse.id}" />
					</c:url>
					
					<!-- construct an "enrolled" link with course id -->
					<c:url var="enrolledLink" value="/clients/enrolled">
						<c:param name="courseId" value="${tempCourse.id}" />
					</c:url>
					
					<!-- construct an "update" link with course id -->
					<c:url var="updateLink" value="/courses/editCourse">
						<c:param name="courseId" value="${tempCourse.id}" />
					</c:url>

					<!-- construct an "delete" link with course id -->
					<c:url var="deleteLink" value="/courses/delete">
						<c:param name="courseId" value="${tempCourse.id}" />
					</c:url>

					<tr>
						<td>${tempCourse.id}</td>
						<td>${tempCourse.name}</td>
						<td>${tempCourse.code}</td>
						<td>${tempCourse.category}</td>
						<td>${tempCourse.prerequisites}</td>
						<td>${tempCourse.description}</td>
						<td>${tempCourse.outline}</td>
						<td>${tempCourse.include}</td>
						<td>${tempCourse.hours}</td>
						<td>${tempCourse.vendor}</td>
						<td>${tempCourse.level}</td>
						<td>${tempCourse.cost}</td>
						<td>${tempCourse.discount}</td>
						<td><a href="${schedulesLink}"><font color="blue">Schedules</font></a></td>
						<td><a href="${enrolledLink}"><font color="blue">Clients</font></a></td>

						<td><a href="${updateLink}"><img
								src="${pageContext.request.contextPath}/resources/images\wrench.PNG"></a><br>
							<a href="${deleteLink}"
							onclick="if (!(confirm('Are you sure you want to delete this course?'))) return false"><img
								src="${pageContext.request.contextPath}/resources/images\delete.PNG"></a></td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>