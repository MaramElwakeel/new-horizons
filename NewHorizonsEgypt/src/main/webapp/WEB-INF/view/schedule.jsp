<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Schedules</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">

<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/Style.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/rtl.css">

</head>
<body>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery-3.3.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">

	<nav class="navbar navbar-expand-lg  navbar-light ">

		<a href="/home"><img
			src="${pageContext.request.contextPath}/resources/images\small.PNG"></a>

		<form class="form-inline foat-right" id="search">
			<input class="form-control " type="search" placeholder="Search"
				aria-label="Search">
			<button class="btn btn-outline-light " type="submit">Search</button>
		</form>

	</nav>
	<div class="table">
		<table class=" table-striped table-bordered">
			<thead class="thead">
				<!-- construct an "add" link with course id -->
				<c:url var="addLink" value="/courses/schedules/addSchedule">
					<c:param name="scheduleId" value="${tempCourse.id}" />
				</c:url>
				<tr>
					<th scope="col">ID</th>
					<th scope="col"><b>Course</b></th>
					<th scope="col"><b>Branch</b></th>
					<th scope="col"><b>Lab</b></th>
					<th scope="col"><b>Start Date</b></th>
					<th scope="col"><b>End Date</b></th>
					<th scope="col"><b>Time</b></th>
					<th scope="col"><a href="${addLink}"><img
							src="${pageContext.request.contextPath}/resources/images\add.PNG"></a></th>
				</tr>
			</thead>
			<tbody>

				<!-- loop over and print our courses -->
				<c:forEach var="tempSchedule" items="${Schedules}">

					<!-- construct an "update" link with course id -->
					<c:url var="updateLink" value="/courses/schedules/editSchedule">
						<c:param name="scheduleId" value="${tempSchedule.id}" />
					</c:url>

					<!-- construct an "delete" link with course id -->
					<c:url var="deleteLink" value="/courses/schedules/delete">
						<c:param name="scheduleId" value="${tempSchedule.id}" />
						<c:param name="courseId" value="${tempSchedule.course_id}" />
					</c:url>

					<tr>
						<td>${tempSchedule.id}</td>
						<td>${tempSchedule.course_id}</td>
						<td>${tempSchedule.branchName}</td>
						<td>${tempSchedule.labNum}</td>
						<td>${tempSchedule.startDate}</td>
						<td>${tempSchedule.endDate}</td>
						<td>${tempSchedule.start_time}</td>

						<td><a href="${updateLink}"><img
								src="${pageContext.request.contextPath}/resources/images\wrench.PNG"></a><br>
							<a href="${deleteLink}"
							onclick="if (!(confirm('Are you sure you want to delete this schedule?'))) return false"><img
								src="${pageContext.request.contextPath}/resources/images\delete.PNG"></a></td>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>