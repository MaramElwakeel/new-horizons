<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>

<head>
	<title>Save Customer</title>

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/style.css">

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/add-customer-style.css">
</head>

<body>
	
	<div id="wrapper">
		<div id="header">
			<h2>CM - Courses Manager</h2>
		</div>
	</div>

	<div id="container">
		<h3>Save Course</h3>
	
		<form:form action="saveCourse" modelAttribute="course" method="POST">

			<!-- need to associate this data with course id -->
			<form:hidden path="id" />
					
			<table>
				<tbody>
					<tr>
						<td><label>Course name:</label></td>
						<td><form:input path="name" /></td>
					</tr>
				
					<tr>
						<td><label>Course Code:</label></td>
						<td><form:input path="code" /></td>
					</tr>

					<tr>
						<td><label>Category:</label></td>
						<td><form:input path="category" /></td>
					</tr>
					
					<tr>
						<td><label>Description:</label></td>
						<td><form:input path="description" /></td>
					</tr>
					
					<tr>
						<td><label>Outlines:</label></td>
						<td><form:input path="outline" /></td>
					</tr>
					
					<tr>
						<td><label>Hours:</label></td>
						<td><form:input path="hours" /></td>
					</tr>
					
					
					<tr>
						<td><label>Level:</label></td>
						<td><form:input path="level" /></td>
					</tr>
					
					<tr>
						<td><label>Cost:</label></td>
						<td><form:input path="cost" /></td>
					</tr>
					
					<tr>
						<td><label>Discount:</label></td>
						<td><form:input path="discount" /></td>
					</tr>

					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>
					
					

				
				</tbody>
			</table>
		
		
		</form:form>
	
		<div style="clear; both;"></div>
		
		<p>
			<a href="${pageContext.request.contextPath}/courses/all">Back to List</a>
		</p>
	
	</div>

</body>

</html>










