package com.newhorizons.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.newhorizons.model.Client;
import com.newhorizons.model.Sales;

@Repository
public class SalesImp implements SalesInterface {
	Connection conn;
	CallableStatement callableStatment;
	PreparedStatement preparedStatement;
	ResultSet resultSet;
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void addSales(Sales sales) {
		// TODO Auto-generated method stub
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql = "INSERT INTO `person`(`name`, `phone`, `email`) VALUES (?, ?,?);";
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setString(1, sales.getName());
			preparedStatement.setString(2, sales.getPhone());
			preparedStatement.setString(3, sales.getEmail());
			
			callableStatment = conn.prepareCall("{call add_new_sales}");
			preparedStatement.executeUpdate();
			callableStatment.executeQuery();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	@Override
	public void deleteSales(int id) {
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql = "DELETE FROM `person` WHERE `id` = ?";
			preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public void updateSales(Sales sales) {
		
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql="UPDATE `person` SET `name`=?,`phone`=?,`email`=? WHERE id =?;";
			preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setString(1, sales.getName());
			preparedStatement.setString(2, sales.getPhone());
			preparedStatement.setString(3, sales.getEmail());
			preparedStatement.setInt(4, sales.getId());
			
			preparedStatement.executeUpdate();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public Sales getSales(int id) {
		Sales sales = new Sales();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_sales_by_ID(?)}");
			callableStatment.setInt(1, id);
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				sales.setId(resultSet.getInt(1));
				sales.setName(resultSet.getString(2));
				sales.setPhone(resultSet.getString(3));
				sales.setEmail(resultSet.getString(4));
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sales;
	}
	
	@Override
	public List<Sales> getSales() {
		List<Sales> salesList = new ArrayList();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_all_sales}");
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				Sales sales = new Sales();
				sales.setId(resultSet.getInt(1));
				sales.setName(resultSet.getString(2));
				sales.setPhone(resultSet.getString(3));
				sales.setEmail(resultSet.getString(4));

				salesList.add(sales);

			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return salesList;
	}
	@Override
	public Sales searchByPhone(String phone) {
		Sales sales = new Sales();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_sales_by_phone(?)}");
			callableStatment.setString(1, phone);
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				sales.setId(resultSet.getInt(1));
				sales.setName(resultSet.getString(2));
				sales.setPhone(resultSet.getString(3));
				sales.setEmail(resultSet.getString(4));
			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sales;
	}

}
