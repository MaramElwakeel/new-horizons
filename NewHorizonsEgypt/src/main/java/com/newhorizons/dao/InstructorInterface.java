package com.newhorizons.dao;

import java.util.List;

import com.newhorizons.model.Client;
import com.newhorizons.model.Instructor;
import com.newhorizons.model.Person;

public interface InstructorInterface {
	public void addInstructor(Instructor instructor);
	public void deleteInstructor(int id);
	public void updateInstructor( Instructor instructor);
	public Instructor getInstructor(int id);
	public List<Instructor> getInstructors();
	public Instructor searchByPhone(String phone);
}
