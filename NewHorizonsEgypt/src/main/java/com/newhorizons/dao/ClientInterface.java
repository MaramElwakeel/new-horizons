package com.newhorizons.dao;

import java.util.List;

import com.newhorizons.model.Client;
import com.newhorizons.model.Person;

public interface ClientInterface {

	void addClient(Client client);

	void deleteClient(int id);

	void updateClient(Client client);

	Client getClient(int id);

	List<Client> getClients();

	List<Client> getEnrolledClients(int id);

	Client searchByPhone(String phone);
	
	public Client searchByEmailPassword(String email, String password);

	Client signUp(Client client);

}
