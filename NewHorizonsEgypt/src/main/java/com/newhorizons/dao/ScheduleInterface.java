package com.newhorizons.dao;

import java.util.List;

import com.newhorizons.model.Schedule;

public interface ScheduleInterface {

	public void addSchedule(Schedule schedule );
	public void deleteSchedule(int course_id);
	public void updateSchedule(Schedule schedule);
	public List<Schedule> getAllSchedule(int course_id);
	public Schedule getSchedule(int id);
}
