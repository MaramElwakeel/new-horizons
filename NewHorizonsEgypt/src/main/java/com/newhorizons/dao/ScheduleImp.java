package com.newhorizons.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.newhorizons.model.Schedule;

@Repository
public class ScheduleImp implements ScheduleInterface {

	Connection conn;
	CallableStatement cstat;
	PreparedStatement preparedStatement;
	Statement stat;
	ResultSet resultSet;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void addSchedule(Schedule schedule) {
		try {

			conn = jdbcTemplate.getDataSource().getConnection();
			cstat = conn.prepareCall("{call add_new_schedule(?,?,?,?,?,?)}");
			cstat.setInt(1, schedule.getCourse_id());
			cstat.setString(2, schedule.getBranchName());
			cstat.setInt(3, schedule.getLabNum());
			cstat.setDate(4, schedule.getStartDate());
			cstat.setDate(5, schedule.getEndDate());
			cstat.setTime(6, schedule.getStart_time());

			cstat.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteSchedule(int course_id) {

		try {

			conn = jdbcTemplate.getDataSource().getConnection();
			cstat = conn.prepareCall("{call delete_schedule(?)}");
			cstat.setInt(1, course_id);

			cstat.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateSchedule(Schedule schedule) {

		try {

			conn = jdbcTemplate.getDataSource().getConnection();
			cstat = conn.prepareCall("{call update_schedule(?,?,?,?,?,?,?)}");
			cstat.setInt(1, schedule.getId());
			cstat.setInt(2, schedule.getCourse_id());
			cstat.setString(3, schedule.getBranchName());
			cstat.setInt(4, schedule.getLabNum());
			cstat.setDate(5, schedule.getStartDate());
			cstat.setDate(6, schedule.getEndDate());
			cstat.setTime(7, schedule.getStart_time());

			cstat.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Schedule> getAllSchedule(int course_id) {
		List<Schedule> list = new ArrayList();
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			preparedStatement = conn.prepareStatement("SELECT * FROM schedule where course_id=?");

			preparedStatement.setInt(1, course_id);

			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Schedule schedule = new Schedule();
				schedule.setId(resultSet.getInt(1));
				schedule.setCourse_id(resultSet.getInt(2));
				schedule.setBranchName(resultSet.getString(3));
				schedule.setLabNum(resultSet.getInt(4));
				schedule.setStartDate(resultSet.getDate(5));
				schedule.setEndDate(resultSet.getDate(6));
				schedule.setStart_time(resultSet.getTime(7));

				list.add(schedule);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Schedule getSchedule(int id) {
		Schedule schedule = new Schedule();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			cstat = conn.prepareCall("{call get_schedule_by_ID(?)}");
			cstat.setInt(1, id);
			resultSet = cstat.executeQuery();

			while (resultSet.next()) {
				schedule.setId(resultSet.getInt(1));
				schedule.setCourse_id(resultSet.getInt(2));
				schedule.setBranchName(resultSet.getString(3));
				schedule.setLabNum(resultSet.getInt(4));
				schedule.setStartDate(resultSet.getDate(5));
				schedule.setEndDate(resultSet.getDate(6));
				schedule.setStart_time(resultSet.getTime(7));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return schedule;
	}

}
