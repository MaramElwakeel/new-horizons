package com.newhorizons.dao;

import java.util.List;

import com.newhorizons.model.Sales;

public interface SalesInterface {

	void addSales(Sales sales);

	void deleteSales(int id);

	void updateSales(Sales sales);

	Sales getSales(int id);

	List<Sales> getSales();

	Sales searchByPhone(String phone);

}
