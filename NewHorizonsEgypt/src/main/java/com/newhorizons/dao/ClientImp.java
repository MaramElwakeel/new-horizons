package com.newhorizons.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.newhorizons.model.Client;
import com.newhorizons.model.Course;
import com.newhorizons.model.Person;

@Repository
public class ClientImp implements ClientInterface {
	Connection conn;
	CallableStatement callableStatment;
	PreparedStatement preparedStatement;
	ResultSet resultSet;
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void addClient(Client client) {
		// TODO Auto-generated method stub
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql = "INSERT INTO `person`(`name`, `phone`, `email`) VALUES (?, ?,?);";
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setString(1, client.getName());
			preparedStatement.setString(2, client.getPhone());
			preparedStatement.setString(3, client.getEmail());

			callableStatment = conn.prepareCall("{call add_new_client(?,?)}");
			callableStatment.setString(1, client.getPassword());
			callableStatment.setString(2, client.getType());
			preparedStatement.executeUpdate();
			callableStatment.executeQuery();
			
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	@Override
	public void deleteClient(int id) {
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql = "DELETE FROM `person` WHERE `id` = ?";
			preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public void updateClient(Client client) {
		
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql="UPDATE `person` SET `name`=?,`phone`=?,`email`=? WHERE id =?;";
			preparedStatement=conn.prepareStatement(sql);
			preparedStatement.setString(1, client.getName());
			preparedStatement.setString(2, client.getPhone());
			preparedStatement.setString(3, client.getEmail());
			preparedStatement.setInt(4, client.getId());
			
			callableStatment = conn.prepareCall("{call update_client(?,?,?)}");
			callableStatment.setString(1, client.getPassword());
			callableStatment.setString(2, client.getType());
			callableStatment.setInt(3, client.getId());
			
			preparedStatement.executeUpdate();
			callableStatment.executeQuery();
			
			
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public Client getClient(int id) {
		Client client = new Client();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_client_by_ID(?)}");
			callableStatment.setInt(1, id);
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				client.setId(resultSet.getInt(1));
				client.setName(resultSet.getString(2));
				client.setPhone(resultSet.getString(3));
				client.setEmail(resultSet.getString(4));
				client.setPassword(resultSet.getString(5));
				client.setType(resultSet.getString(6));
			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return client;
	}
	
	@Override
	public List<Client> getClients() {
		List<Client> clientsList = new ArrayList();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_all_clients}");
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				Client client = new Client();
				client.setId(resultSet.getInt(1));
				client.setName(resultSet.getString(2));
				client.setPhone(resultSet.getString(3));
				client.setEmail(resultSet.getString(4));
				client.setPassword(resultSet.getString(5));
				client.setType(resultSet.getString(6));

				clientsList.add(client);

			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientsList;
	}
	@Override
	public List<Client> getEnrolledClients(int id) {
		List<Client> clientsList = new ArrayList<>();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_enrolled(?)}");
			callableStatment.setInt(1, id);
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				Client client = new Client();
				client.setId(resultSet.getInt(1));
				client.setName(resultSet.getString(2));
				client.setPhone(resultSet.getString(3));
				client.setEmail(resultSet.getString(4));
				client.setPassword(resultSet.getString(5));
				client.setType(resultSet.getString(6));
				clientsList.add(client);
			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientsList;

	}
	@Override
	public Client searchByPhone(String phone) {
		Client client = new Client();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_client_by_phone(?)}");
			callableStatment.setString(1, phone);
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				client.setId(resultSet.getInt(1));
				client.setName(resultSet.getString(2));
				client.setPhone(resultSet.getString(3));
				client.setEmail(resultSet.getString(4));
				client.setPassword(resultSet.getString(5));
				client.setType(resultSet.getString(6));
			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return client;
	}
	
	@Override
	public Client searchByEmailPassword(String email, String password) {
		Client client = new Client();
		try {

			conn = jdbcTemplate.getDataSource().getConnection();

			callableStatment = conn.prepareCall("{call get_client_by_email_password(?, ?)}");
			callableStatment.setString(1, email);
			callableStatment.setString(2, password);
			resultSet = callableStatment.executeQuery();

			while (resultSet.next()) {
				client.setId(resultSet.getInt(1));
				client.setName(resultSet.getString(2));
				client.setPhone(resultSet.getString(3));
				client.setEmail(resultSet.getString(4));
				client.setPassword(resultSet.getString(5));
				client.setType(resultSet.getString(6));
			}
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return client;
	}
	
	@Override
	public Client signUp(Client client) {
		// TODO Auto-generated method stub
		try {
			conn = jdbcTemplate.getDataSource().getConnection();
			String sql = "INSERT INTO `person`(`name`, `phone`, `email`) VALUES (?, ?,?);";
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			preparedStatement.setString(1, client.getName());
			preparedStatement.setString(2, client.getPhone());
			preparedStatement.setString(3, client.getEmail());

			callableStatment = conn.prepareCall("{call add_new_client(?,?)}");
			callableStatment.setString(1, client.getPassword());
			callableStatment.setString(2, client.getType());
			preparedStatement.executeUpdate();
			callableStatment.executeQuery();
			
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return client;
	}

}
