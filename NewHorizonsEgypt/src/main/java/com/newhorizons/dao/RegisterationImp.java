package com.newhorizons.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.newhorizons.model.Registeration;

@Repository
public class RegisterationImp implements RegisterationInterface {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int addRegisteration(Registeration registeration) {

		int st = 0;
		try {
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			String sql = " INSERT INTO registeration(client_id,course_id,schedule_id)" + "values(?,?,?)";
			PreparedStatement stmt = connection.prepareStatement(sql);

			stmt.setInt(1, registeration.getClient_id());
			stmt.setInt(2, registeration.getCourse_id());
			stmt.setInt(3, registeration.getSchedule_id());
			st = stmt.executeUpdate();
			connection.close(); // close the connection
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return st;
	}

	@Override
	public int deleteRegisteration(int schedule_id, int client_id, int course_id) {

		int st = 0;
		String delete = "delete from registeration where schedule_id=? AND course_id=? AND client_id=?";

		try {
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			PreparedStatement stmt = connection.prepareStatement(delete);
			stmt.setInt(1, schedule_id);
			stmt.setInt(2, course_id);
			stmt.setInt(3, client_id);
			st = stmt.executeUpdate();
			connection.close(); // close the connection
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return st;
	}

	@Override
	public Registeration getRegisteration(int schedule_id) {

		String query = "select * from registeration where schedule_id=?";
		Registeration registeration = new Registeration();

		try {
			Connection connection = jdbcTemplate.getDataSource().getConnection();

			PreparedStatement stmt = connection.prepareStatement(query);
			stmt.setInt(1, schedule_id);
			ResultSet resultSet = stmt.executeQuery();
			if (resultSet.next()) {
				registeration.setSchedule_id(resultSet.getInt("schedule_id"));
				registeration.setClient_id(resultSet.getInt("client_id"));
				registeration.setCourse_id(resultSet.getInt("course_id"));
			}
			connection.close(); // close the connection
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return registeration;

	}

	@Override
	public ArrayList<Registeration> getAllRegisteration() {

		String query = "select * from registeration";

		ArrayList<Registeration> list = new ArrayList<>();

		try {
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			PreparedStatement stmt = connection.prepareStatement(query);
			ResultSet resultSet = stmt.executeQuery();
			while (resultSet.next()) {
				Registeration registeration = new Registeration();
				registeration.setClient_id(resultSet.getInt("client_id"));
				registeration.setCourse_id(resultSet.getInt("course_id"));
				registeration.setSchedule_id(resultSet.getInt("schedule_id"));
				list.add(registeration);
			}
			connection.close(); // close the connection
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	
}
