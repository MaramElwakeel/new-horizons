package com.newhorizons.model;

public class Person {
 private int id;	
 private String name;
 private String phone;
 private String email;

 
 	public Person() {
	
}


 	public Person(String name) {
	super();
	this.name = name;
}


	public Person(String name, String phone, String email) {
	super();
	this.name = name;
	this.phone = phone;
	this.email = email;
}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getid() {
		return id;
	}
	
}
