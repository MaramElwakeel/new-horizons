package com.newhorizons.model;

import java.sql.Date;
import java.sql.Time;

public class Schedule {

	private int id;
	private int courseId;
	private Time time;
	private int lab;
	private String branch;
	private Date startDate;
	private Date endDate ;
	
	public Schedule() {
		super();
	}

	public int getId() {
		return id;
	}
	
	public void setId(int Id) {
		this.id = Id;
	}

	public int getLabNum() {
		return lab;
	}

	public void setLabNum(int labNum) {
		this.lab = labNum;
	}

	public String getBranchName() {
		return branch;
	}

	public void setBranchName(String branchName) {
		this.branch = branchName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getCourse_id() {
		return courseId;
	}

	public void setCourse_id(int course_id) {
		this.courseId = course_id;
	}

	public Time getStart_time() {
		return time;
	}

	public void setStart_time(Time start_time) {
		this.time = start_time;
	}
	
	
}
