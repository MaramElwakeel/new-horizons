package com.newhorizons.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.newhorizons.dao.ClientInterface;
import com.newhorizons.dao.InstructorInterface;
import com.newhorizons.model.Client;
import com.newhorizons.model.Instructor;
import com.newhorizons.model.Person;

@Controller
@RequestMapping("/instructors")
public class InstructorController {
	
	// need to inject our clientDAO service
		@Autowired
		private InstructorInterface instructorDAO;
		
		@GetMapping("/home")
		public String home() {
			return "main";
		}
		
		@GetMapping("/all")
		public String getInstructors(Model model) {
			
			// get instructors from the service
			List<Instructor> instructors = instructorDAO.getInstructors();
					
			// add courses to the model
			model.addAttribute("Instructors", instructors);
			
			return ("instructors");
		}
		
		@GetMapping("/addInstructor")
		public String addInstructor(Model model) {
			
			// create model attribute to bind form data
			Instructor instructor = new Instructor();
			
			model.addAttribute("instructor", instructor);
			
			return "addInstructor";
		}
		
		@PostMapping("/saveInstructor")
		public String saveInstructor(@ModelAttribute("instructor") Instructor instructor) {
			
			// save the instructor using our service
			instructorDAO.addInstructor(instructor);	
			
			return "redirect:/instructors/all";
		}
		
		@GetMapping("/editInstructor")
		public String editInstructor(@RequestParam("instructorId") int Id, Model model) {
			
			// get the instructor from our service
			Instructor instructor = instructorDAO.getInstructor(Id);	
			
			// set instructor as a model attribute to pre-populate the form
			model.addAttribute("instructor", instructor);
			
			// send over to our form		
			return "updateInstructor";
		}
		
		@PostMapping("/updateInstructor")
		public String updateInstructor(@ModelAttribute("instructor") Instructor instructor) {
			
			// get the course from our service
			instructorDAO.updateInstructor(instructor);	
			
			// send over to our form		
			return "redirect:/instructors/all";
		}
		
		@GetMapping("/delete")
		public String deleteInstructor(@RequestParam("instructorId") int Id) {
			
			// delete course
			instructorDAO.deleteInstructor(Id);
			
			return "redirect:/instructors/all";
		}
		
		@RequestMapping(value = "/searchPhone", method = RequestMethod.POST)
		public String searchInstructors(@RequestParam("searchPhone") String phone, Model model) {

			// get clients from the service
			Instructor instructor = instructorDAO.searchByPhone(phone);
			List<Instructor> list = new ArrayList<>();
			list.add(instructor);
			// add courses to the model
			model.addAttribute("Instructors", list);

			return ("instructors");
		}

}
