package com.newhorizons.controller;

import java.sql.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.newhorizons.dao.AttendanceInterface;
import com.newhorizons.model.Attendance;

public class AttendanceController {
	@Controller
	@RequestMapping("/attendance")
	public class RegisterationController {

		@Autowired
		private AttendanceInterface attendanceInterface;

		@ResponseBody
		@RequestMapping("/addAttendance")
		public String addAttendance() {

			Attendance attendance = new Attendance();
			attendance.setClient_id(4);

			java.util.Date date = new java.util.Date();
			Date sqlDate = new Date(date.getTime());
			attendance.setDate(sqlDate);

			attendance.setSchedule_id(3);

			int added = attendanceInterface.addAttendance(attendance);
			if (added > 0)
				return "AttendanceADDED";
			return "Attendance NOT ADDED";
		}
	}
}