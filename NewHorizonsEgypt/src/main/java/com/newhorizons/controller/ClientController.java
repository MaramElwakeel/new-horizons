package com.newhorizons.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.newhorizons.dao.ClientInterface;
import com.newhorizons.model.Client;
import com.newhorizons.model.Person;

@Controller
@RequestMapping("/clients")
public class ClientController {

	// need to inject our clientDAO service
	@Autowired
	private ClientInterface clientDAO;

	@GetMapping("/home")
	public String home() {
		return "main";
	}

	@GetMapping("/all")
	public String getClients(Model model) {

		// get clients from the service
		List<Client> clients = clientDAO.getClients();

		// add courses to the model
		model.addAttribute("Clients", clients);

		return ("clients");
	}

	@GetMapping("/addClient")
	public String addClient(Model model) {

		// create model attribute to bind form data
		Client client = new Client();

		model.addAttribute("client", client);

		return "addClient";
	}

	@PostMapping("/saveClient")
	public String saveClient(@ModelAttribute("client") Client client) {

		// save the course using our service
		clientDAO.addClient(client);

		return "redirect:/clients/all";
	}

	@GetMapping("/editClient")
	public String editClient(@RequestParam("clientId") int Id, Model model) {

		// get the client from our service
		Client client = clientDAO.getClient(Id);

		// set client as a model attribute to pre-populate the form
		model.addAttribute("client", client);

		// send over to our form
		return "updateClient";
	}

	@PostMapping("/updateClient")
	public String updateClient(@ModelAttribute("client") Client client) {

		// get the course from our service
		clientDAO.updateClient(client);

		// send over to our form
		return "redirect:/clients/all";
	}

	@GetMapping("/delete")
	public String deleteClient(@RequestParam("clientId") int Id) {

		// delete course
		clientDAO.deleteClient(Id);

		return "redirect:/clients/all";
	}

	@GetMapping("/enrolled")
	public String getEnrolledClients(@RequestParam("courseId") int Id, Model model) {

		// get clients from the service
		List<Client> clients = clientDAO.getEnrolledClients(Id);

		// add courses to the model
		model.addAttribute("Clients", clients);

		return ("enrolled");
	}

	@RequestMapping(value = "/searchPhone", method = RequestMethod.POST)
	public String searchClients(@RequestParam("searchPhone") String phone, Model model) {

		// get clients from the service
		Client client = clientDAO.searchByPhone(phone);
		List<Client> list = new ArrayList<>();
		list.add(client);
		// add courses to the model
		model.addAttribute("Clients", list);

		return ("clients");
	}

}
