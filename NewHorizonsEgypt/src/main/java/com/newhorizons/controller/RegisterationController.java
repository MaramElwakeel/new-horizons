package com.newhorizons.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.newhorizons.dao.RegisterationInterface;
import com.newhorizons.model.Client;
import com.newhorizons.model.Registeration;

@Controller
@RequestMapping("/registeration")
public class RegisterationController {

	@Autowired
	private RegisterationInterface registerationInterface;

	@RequestMapping("/addRegisteration")
	public String addRegisteration(Model model) {

		// create model attribute to bind form data
		Registeration registeration = new Registeration();

		model.addAttribute("registeration", registeration);
		
		return "addRegisteration";
	}

	@PostMapping("/saveRegisteration")
	public String saveRegisteration(@ModelAttribute("registeration") Registeration registeration, RedirectAttributes attributes) {

		// save the course using our service
		registerationInterface.addRegisteration(registeration);

		attributes.addAttribute("courseId", registeration.getCourse_id());
		return "redirect:/clients/enrolled";
	}

	@RequestMapping("/delete")
	public String delete(Model model) {
		Registeration reg = new Registeration();
		model.addAttribute("registeration", reg);
		return "deleteRegisteration";
	}
	
	@RequestMapping("/deleteRegisteration")
	public String deleteClient(@RequestParam("client_id") int clientId, @RequestParam("course_id") int courseId, @RequestParam("schedule_id") int scheduleId) {

		// delete course
		registerationInterface.deleteRegisteration(scheduleId, clientId, courseId);

		return "redirect:/courses/available";
	}

	@ResponseBody
	@RequestMapping("/getRegisteration")
	public String getRegisteration() {

		Registeration registeration = registerationInterface.getRegisteration(3);

		String show = "ScheduleID=  " + registeration.getSchedule_id() + "\n" + "CourseID= "
				+ registeration.getCourse_id() + "\n" + "ClientID= " + registeration.getClient_id();
		return show;
	}

	@GetMapping("/registerationList")
	public String registerationList(Model model) {

		List<Registeration> registerations = registerationInterface.getAllRegisteration();
		model.addAttribute("Registerations", registerations);

		return "RegisterationList";
	}
	
}
