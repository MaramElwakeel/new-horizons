package com.newhorizons.apicontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.newhorizons.dao.SalesInterface;
import com.newhorizons.model.Sales;

@RestController
@RequestMapping("/apisales")
public class SalesApiController {

	@Autowired
	private SalesInterface salesInterface;
	
	@RequestMapping("/all")
	public List<Sales> getSales() {

		return salesInterface.getSales();		 
	}
	
	@RequestMapping("{id}")
	public Sales getSales(@PathVariable("id") int id) {

		return salesInterface.getSales(id);	 
	}
	
	@RequestMapping("/searchByPhone/{phone}")
	public Sales searchByPhone(@PathVariable("phone") String phone) {

		return salesInterface.searchByPhone(phone); 
	}
	
}
