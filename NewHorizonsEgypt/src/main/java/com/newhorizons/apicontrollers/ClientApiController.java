package com.newhorizons.apicontrollers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.newhorizons.model.Client;
import com.newhorizons.dao.ClientInterface;

@RestController
@RequestMapping("/apiclient")
public class ClientApiController {

	@Autowired
	private ClientInterface clientInterface;

	@RequestMapping("/all")
	public List<Client> getClients() {

		return clientInterface.getClients();
	}

	@RequestMapping("/clientid/{id}")
	public Client getClient(@PathVariable("id") int id) {
		return clientInterface.getClient(id);
	}

	@RequestMapping("/clientsEnrolled/{courseid}")
	public List<Client> getEnrolledClient(@PathVariable("courseid") int id) {
		return clientInterface.getEnrolledClients(id);
	}

	@RequestMapping("/searchByPhone/{phone}")
	public Client getclientByPhone(@PathVariable("phone") String phone) {
		return clientInterface.searchByPhone(phone);
	}

	@RequestMapping(method = RequestMethod.POST, value = ("/signup"), consumes = "application/json")
	public Client signup(@RequestBody Client client) {
		clientInterface.addClient(client);
		return client;
	}

	@RequestMapping(method = RequestMethod.PUT, value = ("/updateclient"))
	public void updateClient(@RequestBody Client client) {
		clientInterface.updateClient(client);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = ("/deleteclient"))
	public void deleteClient(@RequestBody int id) {
		clientInterface.deleteClient(id);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/signin")
	public Client getClient(@RequestParam String email, @RequestParam String password) {
		return clientInterface.searchByEmailPassword(email, password);
	}

}
