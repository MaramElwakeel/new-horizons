package com.newhorizons.apicontrollers;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.newhorizons.dao.CourseInterface;
import com.newhorizons.model.Course;

@RestController
@RequestMapping("/apicourse")
public class CoursesApiController {

	@Autowired
	private CourseInterface courseInterface;
	
	@RequestMapping("/all")
	public List<Course> getCourses() {

		return courseInterface.getAvailableCourses();		 
	}
	
	@RequestMapping("/courseByBranch/{branch}")
	public List<Course> getCoursesByBranch(@PathVariable("branch") String branch) {

		return courseInterface.getCourseByBranch(branch);		 
	}
	@RequestMapping("/courseByCategory/{category}")
	public List<Course> getCoursesByCategory(@PathVariable("category") String category) {

		return courseInterface.getCourseByCategory(category);		 
	}
	
	@RequestMapping("/courseByCode/{code}")
	public List<Course> getCoursesByCode(@PathVariable("code") String code) {

		return courseInterface.getCourseByCode(code);	 
	}
	
	@RequestMapping("/courseByVendor/{vendor}")
	public List<Course> getCoursesByVendor(@PathVariable("vendor") String vendor) {

		return courseInterface.getCourseByVendor(vendor); 
	}
	
	@RequestMapping(method=RequestMethod.POST,value=("/addcourse"))
	public void addCourse(@RequestBody Course course) {
		courseInterface.addCourse(course);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value=("/deletecourse"))
	public void deleteCourse(@RequestBody int id) {
		courseInterface.deleteCourse(id);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value=("/updatecourse"))
	public void updateCourse(@RequestBody Course course) {
		courseInterface.updateCourse(course);
	}
	
	@RequestMapping("/vendors")
	public List<String> getVendors() {

		return courseInterface.getVendors(); 
	}
	
	@RequestMapping("/categories")
	public List<String> getCategories() {

		return courseInterface.getCategories();
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/enrolled/{clientid}")
	public List<Course> getEnrolledCourses(@PathVariable("clientid") int clientID) {

		return courseInterface.getEnrolledCourses(clientID);	 
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/finished/{clientid}")
	public List<Course> getFinishedCourses(@PathVariable("clientid") int clientID) {

		return courseInterface.getFinishedCourses(clientID);	 
	}
	
	@RequestMapping(method=RequestMethod.GET, value= "/search/{value}")
	public List<Course> getFinishedCourses(@PathVariable("value") String value) {

		return courseInterface.searchCourses(value) ;
	}
}
